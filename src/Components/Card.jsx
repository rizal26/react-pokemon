import React from "react";
const Card = ({ pokemon, loading ,infoPokemon}) => {
   console.log(pokemon);
    return (
        <>
        {
            loading ? <h1>Loading...</h1> :
                pokemon.map((item) => {
                    const style = `card ${item.types[0].type.name}`
                    return (
                        <>
                            <div className={style}  key={item.id} onClick={()=>infoPokemon(item)}>
                                <h2 className="order">#{item.id}</h2>
                                <img className="thumb-img" src={item.sprites.front_default} alt="" />
                                <h2>{item.name}</h2>
                                <div className="abilities-left">
                                    {
                                        item.abilities.map((val) => {
                                            return (
                                                <div className="group">
                                                    <span>{val.ability.name}</span>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </>
                    )
                })
        }

        </>
    )
}
export default Card;