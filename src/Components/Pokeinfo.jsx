import React from "react";

const Pokeinfo = ({ data }) => {
   
    return (
        <>
        {
            (!data) ? "" : (
                <>
                    <h1>{data.name}</h1>
                    <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${data.id}.svg`} alt="" />
                    <div className="abilities">
                        {
                            data.abilities.map(poke=>{
                                return(
                                    <>
                                     <div className="group">
                                        <h2>{poke.ability.name}</h2>
                                    </div>
                                    </>
                                )
                            })
                        }
                    </div>
                    <div className="base-stat">
                        <table style={{ 'width': '100%' }}>
                        {
                            data.stats.map(poke=>{
                                return(
                                    <>
                                        <tr>
                                            <td>{poke.stat.name}</td>
                                            <td style={{ 'padding-right': '10px' }}></td>
                                            <td>{poke.base_stat}</td>
                                            <td style={{ 'width': '75%' }}>
                                                <div className="progress">
                                                    <div className="bar" style={{ 'width': `${ (poke.base_stat/120)*100 }%` }}>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        {/* <h3>{poke.stat.name}:{poke.base_stat}</h3> */}
                                    </>
                                )
                            })
                        }
                        </table>
                    </div>
                </>
            )
        }

        </>
    )
}
export default Pokeinfo